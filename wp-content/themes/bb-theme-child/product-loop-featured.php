<div class="product-grid featured col-md-12 col-sm-12 col-xs-12" itemscope itemtype="http://schema.org/CollectionPage">
    <div class="row">
<?php while ( have_posts() ): the_post(); ?>
    <div class="col-md-3 col-sm-6 col-xs-6">
    <!-- <div class="fl-post-grid-post" itemscope itemtype="<?php //FLPostGridModule::schema_itemtype(); ?>"> -->
    <div class="fl-post-grid-post" itemscope itemtype="Product">
        <?php FLPostGridModule::schema_meta(); ?>
        <?php if(get_field('room_scene_image_link')) { ?>
            <div class="fl-post-grid-image">

                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
				
                    <div class="grid-room" style=" position: relative; width: 100%; height: 250px; background: url(http:<?php the_field('swatch_image_link'); ?>?$sf_carpet_results_swatch$&scl=6&wid=250&hei=250&fit=crop) no-repeat bottom center; background-size: cover;">

                    <?php //the_post_thumbnail($settings->image_size); ?>
                    <!-- <img src="<?php //the_field('room_scene_image_link'); ?>" alt="<?php //the_title_attribute(); ?>" /> -->
                    </div>
                </a>

            </div>
        <?php } else { ?>
            <div class="fl-post-grid-image">

                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
                    <img src="http://placehold.it/300x500?text=No+Image" alt="<?php the_title_attribute(); ?>" />
                </a>

            </div>

        <?php } ?>
        <div class="fl-post-grid-text product-grid btn-grey">

            <h4><?php the_field('style'); ?></h4>

            <h2 class="fl-post-grid-title" itemprop="headline">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php //the_title(); ?><?php the_field('color'); ?></a>
            </h2>
            <?php if(get_field('retail_price')) { ?>
<span class="strike-price">$<?php the_field('retail_price'); ?></span>
            <?php } ?>

            <?php if(get_field('sale_price')) { ?>
                <span class="sale-price">$<?php the_field('sale_price'); ?> sq. ft.</span>
            <?php } ?>


            <div class="fl-button-wrap fl-button-width-auto fl-button-center">
                <a href="<?php echo site_url(); ?>/flooring-coupon/" target="_self" class="fl-button" role="button">
                    <span class="fl-button-text">GET COUPON</span>
                </a>
            </div>


            <a class="greylink" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">VIEW PRODUCT</a>


        </div>
    </div>
    </div>
<?php endwhile; ?>
</div>
</div>