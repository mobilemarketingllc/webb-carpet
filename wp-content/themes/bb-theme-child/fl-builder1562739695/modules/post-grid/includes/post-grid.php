<?php
$item = get_field('sku');
	$itemImage = explode("_", $item);
	$imageNew= $itemImage[1] .'_'. $itemImage[0];
	
if(get_post_type() == "carpeting") {
	 	$remaining= $post->ID % 9;
		if($remaining == $lastItem && $lastItem < 8){
			$remaining = $remaining + 2;
		}
		elseif($remaining == $lastItem && $lastItem > 8){
			$remaining = $remaining - 1;
		}
	$lastItem =  $remaining;
		if($remaining == 1){
			$item = "20150128_LIVINGROOM_7_SQUARE";
		} elseif($remaining == 2){
			$item = "20130408_LIVINGROOM_3_SQUARE";
		}elseif($remaining == 3){
			$item = "20150504_STAIR_RUNNER_SQUARE";
		}elseif($remaining == 4){
			$item = "20130408_BEDROOM_2_SQUARE";
		}elseif($remaining == 5){
			$item = "20130325_HGTV_LIVINGROOM_1_RECTANGLE";
		}elseif($remaining == 6){
			$item = "20141218_BEDROOM_3_SQUARE";
		}elseif($remaining == 7){
			$item = "20160222_LIVINGROOM_5_SQUARE";
		}elseif($remaining == 8){
			$item = "20130501_COMMERICAL_54593ONLY_RECTANGLE";
		}elseif($remaining == 9){
			$item = "20130122_DININGROOM_1";
		}elseif($remaining == 10){
			$item = "20130408_LIVINGROOM_2_SQUARE";
		}else {
			$item = "20141218_BEDROOM_1_SQUARE";
		}
	
		$a1='//shawfloors.scene7.com/is/image/ShawIndustries/?src=ir(ShawIndustriesRender/'.$item.'?res=20&src=is(ShawIndustriesRender/'.$imageNew.'_MAIN))&qlt=60&wid=300&hei=300&fit=crop,0';
		$a2='//shawfloors.scene7.com/is/image/ShawIndustries/?src=ir(ShawIndustriesRender/'.$item.'?res=20&src=is(ShawIndustriesRender/'.$imageNew.'_MAIN))&qlt=60&wid=600&hei=400&fit=crop,0';
		

		//Code to check the image exits.	
		$url ='https://shawfloors.scene7.com/is/image/ShawIndustriesRender/'.$imageNew.'_MAIN?req=exists,text';
	
	
	
	} 
	else{
		$a1 = "http://shawfloors.scene7.com/is/image/ShawIndustries/". $imageNew ."_ROOM?fmt=pjpeg&fit=crop&qlt=60&wid=300&hei=300";
		$a2= "http://shawfloors.scene7.com/is/image/ShawIndustries/". $imageNew ."_ROOM?fmt=pjpeg&fit=crop&qlt=60&wid=600&hei=400";
		//Code to check the image exits.	
		$url ='https://shawfloors.scene7.com/is/image/ShawIndustries/'.$imageNew.'_ROOM?req=exists,text';
	} 
	
	
//Code to check the image exits.	
$response = wp_remote_post($url, array('method' => 'GET'));		
$ImageExist = $response["body"];	
$exits = strpos($ImageExist,"catalogRecord.exists=0");		
if ($exits === false) {					
					
		
?>
<div <?php post_class('fl-post-grid-post open-gallery-modal'); ?> itemscope itemtype="<?php FLPostGridModule::schema_itemtype(); ?>">
	
	<?php FLPostGridModule::schema_meta(); ?>

	<?php if(has_post_thumbnail() && $settings->show_image) : ?>
	<div class="fl-post-grid-image">
		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
			<?php the_post_thumbnail($settings->image_size); ?>
		</a>
	</div>
	<?php endif; ?>
	
		
	<div class="fl-post-grid-gallery" style="background: url('<?php echo $a1; ?>') no-repeat;background-size: cover;">
	</div>
	


	<div class="gallery-modal">
		<div class="fl-row-content-wrap">
			<div class="content">
				<div class="col col-sm-6 col-md-8 col-lg-8 col_swatch" style="background-image:url('<?php echo $a2; ?>')">
					&nbsp;
				</div>
				<div class="col col-sm-6 col-md-4 col-lg-4 product-box">
					<a href="#" class="close_modal"><i class="fa fa-times"></i></a>
	                <h2 class="collection"><?php the_field('collection'); ?></h2>
	                <h1 class="fl-post-title" itemprop="name">
	                   <?php the_field('color'); ?>
	                </h1>

	                <div class="product-colors">
	                    <?php
						global $post;
						 
	                    $familysku = get_post_meta($post->ID, 'collection', true);						
	                    if(is_singular( 'laminate_catalog' )){
                        $flooringtype = 'laminate_catalog';
                    } elseif(is_singular( 'hardwood' )){
                        $flooringtype = 'hardwood';
                    } elseif(is_singular( 'carpeting' )){
                        $flooringtype = 'carpeting';
                    } elseif(is_singular( 'luxury_vinyl_tile' )){
                        $flooringtype = 'luxury_vinyl_tile';
                    } elseif(is_singular( 'vinyl' )){
                        $flooringtype = 'vinyl';
                    } elseif(is_singular( 'solid_wpc_waterproof' )){
                        $flooringtype = 'solid_wpc_waterproof';
                    } elseif(is_singular( 'tile_catalog' )){
                        $flooringtype = 'tile_catalog';
                    } ?>


                        <?php if (is_page( 19677 )) { ?>
                            <?php
                            $args = array(
                                'post_type'      => array( 'carpeting', 'hardwood', 'laminate', 'luxury_vinyl_tile', 'vinyl', 'tile', 'solid_wpc_waterproof'  ),
                                'posts_per_page' => -1,
                                'post_status'    => 'publish',
                                'meta_query' => array(
                                    array(
                                        'key' => 'in_stock',
                                        'compare' => '==',
                                        'value' => '1'
                                    ))
                            );
                            ?>

                       <?php  } else { ?>
                            <?php
                            $args = array(
                                'post_type'      => $flooringtype,
                                'posts_per_page' => -1,
                                'post_status'    => 'publish',
                                'meta_query'     => array(
                                    array(
                                        'key'     => 'collection',
                                        'value'   => $familysku,
                                        'compare' => '='
                                    )
                                )
                            );
                            ?>

                       <?php } ?>







	                    <?php
	                 
						
						 $the_query = new WP_Query( $args );
					
	                    ?>
	                    <?php  //echo $the_query ->found_posts; ?> <!-- Colors Available -->
	                </div>

					<br>
	                <a href="<?php the_permalink() ?>" class="view_more fl-button"><span class="fl-button-text"><?php _e("View More","fl-builder"); ?></span></a>
					<br>
	                <a href="<?php echo get_permalink(13) ?>" class="contact_us fl-button"><span class="fl-button-text"><?php _e("Contact Us","fl-builder"); ?></span></a>
				</div>
			</div>
		</div>
	</div>

</div>
<?php } ?>