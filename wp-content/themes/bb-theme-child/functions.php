<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

// // SS - Coupon - $500oFF
add_action( 'gform_after_submission_14', 'post_to_third_party_14', 10, 2 );

function post_to_third_party_14( $entry, $form ) {
    $baseURI = 'https://app-3QNH5ZKKGG.marketingautomation.services/webforms/receivePostback/MzawMDE3MjUwAgA/';
    $endpoint = 'dc35b4a4-a892-4a8b-840d-ff2b161bd83b';
    $post_url = $baseURI . $endpoint;

    $body = array(
        'first' => rgar( $entry, '4' ),
        'last' => rgar( $entry, '5' ),
        'email' => rgar( $entry, '6' ),
        'phone' => rgar( $entry, '2' ),
        'terms' => rgar( $entry, '7' ),
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}

//SS-contact us
add_action( 'gform_after_submission_15', 'post_to_third_party_15', 10, 2 );

function post_to_third_party_15( $entry, $form ) {
    $baseURI = 'https://app-3QNH5ZKKGG.marketingautomation.services/webforms/receivePostback/MzawMDE3MjUwAgA/';
    $endpoint = '23b20b7d-7053-4fe1-bd29-60aa4c3e0cf7';
    $post_url = $baseURI . $endpoint;

    $body = array(
        'first' => rgar( $entry, '1' ),
        'last' => rgar( $entry, '2' ),
        'email' => rgar( $entry, '3' ),
        'number' => rgar( $entry, '4' ),
        'questions' => rgar( $entry, '10' ),
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}

//SS - Carpet Cleaning Lead
add_action( 'gform_after_submission_17', 'post_to_third_party_17', 10, 2 );

function post_to_third_party_17( $entry, $form ) {
    $baseURI = 'https://app-3QNH5ZKKGG.marketingautomation.services/webforms/receivePostback/MzawMDE3MjUwAgA/';
    $endpoint = 'f03832c9-db0f-48f8-a79a-1d50c5886f32';
    $post_url = $baseURI . $endpoint;

    $body = array(
        'first' => rgar( $entry, '1' ),
        'last' => rgar( $entry, '2' ),
        'email' => rgar( $entry, '3' ),
        'phone' => rgar( $entry, '4' ),
        'zip' => rgar( $entry, '5' ),
        'rooms' => rgar( $entry, '11' ),
        'steps' => rgar( $entry, '14' ),
        'date' => rgar( $entry, '13' ),
        'time' => rgar( $entry, '12' ),
        'comments' => rgar( $entry, '10' ),
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}

// SS - hardwood restoration lead
add_action( 'gform_after_submission_16', 'post_to_third_party_16', 10, 2 );

function post_to_third_party_16( $entry, $form ) {
    $baseURI = 'https://app-3QNH5ZKKGG.marketingautomation.services/webforms/receivePostback/MzawMDE3MjUwAgA/';
    $endpoint = '73102e16-aa51-42f3-a9ec-ae6361c3f7a2';
    $post_url = $baseURI . $endpoint;

    $body = array(
        'first' => rgar( $entry, '1' ),
        'last' => rgar( $entry, '2' ),
        'email' => rgar( $entry, '3' ),
        'phone' => rgar( $entry, '4' ),
        'rooms' => rgar( $entry, '11' ),
        'date' => rgar( $entry, '13' ),
        'time' => rgar( $entry, '12' ),
        'comments' => rgar( $entry, '10' ),
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}

// SS - Measurement Lead
add_action( 'gform_after_submission_3', 'post_to_third_party_3', 10, 2 );

function post_to_third_party_3( $entry, $form ) {
    $baseURI = 'https://app-3QNH5ZKKGG.marketingautomation.services/webforms/receivePostback/MzawMDE3MjUwAgA/';
    $endpoint = '8d469749-7a36-4b6d-8384-5874d867f396';
    $post_url = $baseURI . $endpoint;

    $body = array(
        'first' => rgar( $entry, '1' ),
        'last' => rgar( $entry, '2' ),
        'email' => rgar( $entry, '3' ),
        'phone' => rgar( $entry, '4' ),
        'zip' => rgar( $entry, '5' ),
        'product type' => rgar( $entry, '11' ),
        'date' => rgar( $entry, '13' ),
        'time' => rgar( $entry, '12' ),
        'comments' => rgar( $entry, '10' ),
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}

// SS - Military Lead
add_action( 'gform_after_submission_18', 'post_to_third_party_18', 10, 2 );

function post_to_third_party_18( $entry, $form ) {
    $baseURI = 'https://app-3QNH5ZKKGG.marketingautomation.services/webforms/receivePostback/MzawMDE3MjUwAgA/';
    $endpoint = 'e5bc60c2-4f61-4d60-ac55-50d63c118386';
    $post_url = $baseURI . $endpoint;

    $body = array(
        'first' => rgar( $entry, '4' ),
        'last' => rgar( $entry, '5' ),
        'email' => rgar( $entry, '6' ),
        'phone' => rgar( $entry, '2' ),
        'terms' => rgar( $entry, '7' ),
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_style("slick",get_stylesheet_directory_uri()."/resources/slick/slick.css");
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
});


// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );



//* Remove Query String from Static Resources
function remove_css_js_ver( $src ) {
    if( strpos( $src, '?ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'remove_css_js_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_css_js_ver', 10, 2 );


// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');


// Facetwp results
add_filter( 'facetwp_result_count', function( $output, $params ) {
    //$output = $params['lower'] . '-' . $params['upper'] . ' of ' . $params['total'] . ' results';
    $output =  $params['total'] . ' Products';
    return $output;
}, 10, 2 );
// Facetwp results pager
function my_facetwp_pager_html( $output, $params ) {
    $output = '';
    $page = $params['page'];
    $total_pages = $params['total_pages'];
    if ( $page > 1 ) {
        $output .= '<a class="facetwp-page" data-page="' . ($page - 1) . '"><span class="pager-arrow"><</span></a>';
    }
    $output .= '<span class="pager-text">page ' . $page . ' of ' . $total_pages . '</span>';
    if ( $page < $total_pages && $total_pages > 1 ) {
        $output .= '<a class="facetwp-page" data-page="' . ($page + 1) . '"><span class="pager-arrow">></span></a>';
    }
    return $output;
}

add_filter( 'facetwp_pager_html', 'my_facetwp_pager_html', 10, 2 );


// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');


function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}

//Disable gravity forms editor
function remove_gf_notification_visual_editor($settings, $editor_id)
{
    if ($editor_id === 'gform_notification_message') {
    $settings['tinymce'] = false;
}
    return $settings;
}

add_filter('wp_editor_settings', 'remove_gf_notification_visual_editor', 10, 2);


add_action( 'wp_head', function() {
?>
<script>
(function($) {
  $(document).on('facetwp-refresh', function() {
    if (FWP.loaded) {
      FWP.set_hash();
      window.location.reload();
      return false;
    }
  });
})(jQuery);
</script>
<?php
}, 100 );
remove_action( 'wp_head', 'feed_links_extra', 3 );

// Action to for styling H1 tag - google keyword fields
add_action('wp_head', 'add_css_head');
function add_css_head() {
    
   ?>
      <style>
          .googlekeyword {
             text-align:center;
             color: #fff;
             text-transform: capitalize;   
			  font-size:2.5em !important;
           }
      </style>  
   <?php    
}

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );


/*

add_filter( 'gform_field_validation_3_13', 'custom_date_validation', 10, 4 );
function custom_date_validation( $result, $value, $form, $field ) {

    if ( $result['is_valid'] && $field->get_input_type() == 'date' ) {
       
        $date = GFCommon::parse_date( $value );

        if ( ! GFCommon::is_empty_array( $date ) && checkdate( $date['month'], $date['day'], $date['year'] ) ) {
             $today = date("m/d/Y");
             $date_now = strtotime($today. ' + 2 days');
             $date2    = strtotime($value);
             if ($date2 >= $date_now) {
                $result['is_valid'] = true;
                $result['message']  = '';
             }else{
                $result['is_valid'] = false;
                $result['message']  = 'Estimate date should be 2 days greater than today';
             }
           
        } else {
            $result['message'] = 'Please enter a valid date.';
        }
    }
 
    return $result;
}*/

//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_override_yoast_breadcrumb_trail',10 );

function wpse_override_yoast_breadcrumb_trail( $links ) {

    if (is_singular( 'luxury_vinyl_tile' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/luxury-vinyl/',
            'text' => 'Luxury Vinyl',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/luxury-vinyl/products/',
            'text' => 'Luxury Vinyl Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
    
    return $links;
}

?>