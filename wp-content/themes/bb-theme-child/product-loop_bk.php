<div class="product-grid swatch" itemscope itemtype="http://schema.org/CollectionPage">
    <div class="row product-row">
<?php while ( have_posts() ): the_post(); ?>
    <div class="col-md-3 col-sm-4 col-xs-6">
    <!-- <div class="fl-post-grid-post" itemscope itemtype="<?php //FLPostGridModule::schema_itemtype(); ?>"> -->
    <div class="fl-post-grid-post" itemscope itemtype="Product">
        <?php FLPostGridModule::schema_meta(); ?>
        <?php if(get_field('swatch_image_link')) { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
  				<?php 
												
						if(get_post_type() =='carpeting') {
							$item = get_field('sku');
							$itemImage = explode("_", $item);	
							$imageNew= $itemImage[1] .'_'. $itemImage[0];
							$image = "http://shawfloors.scene7.com/is/image/ShawIndustries/".$imageNew."_SWATCH?fmt=pjpeg&fit=crop&wid=322&hei=322";
							
							$url ='https://shawfloors.scene7.com/is/image/ShawIndustries/'.$imageNew.'_SWATCH?req=exists,text';
							$response = wp_remote_post($url, array('method' => 'GET'));		
							$ImageExist = $response["body"];	
							$exits = strpos($ImageExist,"catalogRecord.exists=0");		
							if ($exits !== false) {	 
								$image = "http://shawfloors.scene7.com/is/image/ShawIndustries/".$imageNew."_MAIN?fmt=pjpeg&fit=crop&wid=322&hei=322";
								$url ='https://shawfloors.scene7.com/is/image/ShawIndustries/'.$imageNew.'_MAIN?req=exists,text';
								$response = wp_remote_post($url, array('method' => 'GET'));		
								$ImageExist = $response["body"];	
								$exits = strpos($ImageExist,"catalogRecord.exists=0");		
								if ($exits !== false) {	 
									$image = "http://shawfloors.scene7.com/is/image/ShawIndustriesRender/".$imageNew."_MAIN?fmt=pjpeg&fit=crop&wid=322&hei=322";
								}
							}
							
							$class = "";
						} else {
							
							$itemImage = get_field('swatch_image_link');
							$image= $itemImage . "?fmt=jpg&qlt=60&hei=222&wid=222&fit=crop,0";
							$class = "shadow";
						}
					?>
            <img class="<?php echo $class; ?>" src="<?php  echo $image; ?>" alt="<?php the_title_attribute(); ?>" />
                  
                </a>
            </div>
        <?php } else { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
                   <img class="<?php echo $class; ?>" src="http://placehold.it/168x123?text=COMING+SOON" alt="<?php the_title_attribute(); ?>" />
                </a>
            </div>

        <?php } ?>
        <div class="fl-post-grid-text product-grid btn-grey">
            <h4><?php the_field('style'); ?></h4>
            <h2 class="fl-post-grid-title" itemprop="headline">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php //the_title(); ?><?php the_field('color'); ?></a>
            </h2>
            <a href="<?php echo site_url(); ?>/flooring-coupon/?keyword=<?php echo $_COOKIE['keyword']; ?>&brand=<?php echo get_field('brand');?>" target="_self" class="fl-button" role="button">
                <span class="fl-button-text">GET COUPON</span>
            </a><br />
            <a class="link" href="<?php the_permalink(); ?>">VIEW PRODUCT</a>
        </div>
    </div>
    </div>
<?php endwhile; ?>
</div>
</div>